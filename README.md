cd /src

javac ClassFinder.java

java ClassFinder _filename_ **'searchword'**

e.g. java ClassFinder _classes.txt_ **'FB'**
