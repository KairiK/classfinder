import java.util.List;

public class ClassFinder {

    public static void main(String[] args) {
        FileReader fileReader = new FileReader();
        SorterByPattern sorterByPattern = new SorterByPattern();
        SorterByAlphabet sorterByAlphabet = new SorterByAlphabet();
        Printer printer = new Printer();

        String fileName = args[0];
        String pattern = args[1];

        List<String> classNames = fileReader.read(fileName);
        List<String> sortedClassNames = sorterByPattern.sort(classNames, pattern);

        if (sortedClassNames.size() == 0) {
            System.out.println("No matches found");
        } else {
            List<String> alphabeticallySortedClassNames = sorterByAlphabet.sort(sortedClassNames);
            printer.print(alphabeticallySortedClassNames);
        }
    }
}
