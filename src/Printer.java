import java.util.List;

public class Printer {

    void print (List<String> classNames) {
        for ( String className : classNames) {
            System.out.println(className);
        }
    }
}
