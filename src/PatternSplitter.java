import java.util.ArrayList;
import java.util.List;

class PatternSplitter {
    List<String> splitIntoList(String pattern) {
        List<String> patternParts = new ArrayList<>();
        String patternPartsInOneString = splitWithSpacesIntoOneString(pattern);

        StringBuilder temp = new StringBuilder();

        for (char character : patternPartsInOneString.trim().toCharArray()) {
            if (Character.isLetter(character)) {
                temp.append(character);
            } else {
                if (!"".equals(temp.toString())) {
                    patternParts.add(temp.toString());
                }
                temp = new StringBuilder();
            }
        }
        patternParts.add(temp.toString());
        return patternParts;
    }

    String splitWithSpacesIntoOneString(String pattern) {
        StringBuilder patternPartsSeparateInOneString = new StringBuilder();
        for (int i = 0; i < pattern.length(); i++) {
            Character ch = pattern.charAt(i);
            if (Character.isUpperCase(ch))
                patternPartsSeparateInOneString.append(" ").append(ch);
            else
                patternPartsSeparateInOneString.append(ch);
        }
        return patternPartsSeparateInOneString.toString();
    }

}

