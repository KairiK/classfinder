import java.util.ArrayList;
import java.util.List;

class SorterByAlphabet {

    List<String> sort (List<String> classNames){
        List<String> sortedClassNames = new ArrayList<>();

        for (String className : classNames){
            sortedClassNames.add(className.trim());
        }

        sortedClassNames.sort(String.CASE_INSENSITIVE_ORDER);
        return sortedClassNames;
    }
}
