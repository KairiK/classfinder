import java.util.ArrayList;
import java.util.List;

class SorterByPattern {
    private PatternSplitter splitter = new PatternSplitter();

    List<String> sort(List<String> classNames, String pattern) {
        List<String> matchingClassNames;
        boolean patternEndsWithSpace = pattern.endsWith(" ");
        List<String> patternParts = splitter.splitIntoList(pattern);

        if (isOnlyLowerCase(pattern)) {
            matchingClassNames = getMatchingLowerCaseNames(classNames, pattern);
        } else {
            matchingClassNames = getMatchingCamelCaseNames(classNames, patternParts, patternEndsWithSpace);
        }
        return matchingClassNames;
    }

    boolean isOnlyLowerCase(String pattern) {
        return pattern.equals(pattern.toLowerCase());
    }

    boolean charExistsInString(char character, String className) {
        return className.indexOf(character) >= 0;
    }

    boolean patternPartExistsInString(String patternPart, String className) {
        return className.contains(patternPart);
    }

    int getPositionOfWildcardPartInString(String patternPart, String className) {
        for (int i = 0; i <= (className.length() - patternPart.length()); i++) {
            int j;

            for (j = 0; j < patternPart.length(); j++) {
                if (className.charAt(i + j) != patternPart.charAt(j) && patternPart.charAt(j) != '*') {
                    break;
                }
            }

            if (j == patternPart.length()) {
                return i;
            }
        }

        return -1;
    }

    List<String> getMatchingCamelCaseNames(List<String> classNames, List<String> patternParts, boolean mustEndWithPatternWord) {
        List<String> matchingClassNames = new ArrayList<>();
        for (String className : classNames) {
            if (doesClassNameMatchCamelCasePattern(className, patternParts, mustEndWithPatternWord)) {
                matchingClassNames.add(className);
            }
        }
        return matchingClassNames;
    }

    List<String> getMatchingLowerCaseNames(List<String> classNames, String pattern) {
        List<String> matchingClassNames = new ArrayList<>();
        for (String className : classNames) {
            String lowerCaseClassName = className.toLowerCase();
            if (doesClassNameMatchLowerCasePattern(lowerCaseClassName, pattern)) {
                matchingClassNames.add(className);
            }
        }
        return matchingClassNames;
    }

    boolean doesClassNameMatchLowerCasePattern(String className, String pattern) {
        String classNamePart = className;
        for (char character : pattern.toCharArray()) {
            if (charExistsInString(character, classNamePart)) {
                int charPositionInName = classNamePart.indexOf(character);
                int positionOfLastCharInName = classNamePart.length() - 1;
                if (charPositionInName != positionOfLastCharInName) {
                    classNamePart = classNamePart.substring(charPositionInName + 1);
                } else {
                    classNamePart = "";
                }
            } else {
                return false;
            }
        }
        return true;
    }

    boolean doesClassNameMatchCamelCasePattern(String className, List<String> patternParts, boolean mustEndWithPatternWord) {
        String classNamePart = className;

        for (String part : patternParts) {
            if (!containsWildcard(part)) {
                if (patternPartExistsInString(part, classNamePart)) {
                    int positionOfLastCharOfPatternInClassName = classNamePart.indexOf(part) + part.length();
                    if (positionOfLastCharOfPatternInClassName != classNamePart.length() - 1) {
                        classNamePart = classNamePart.substring(positionOfLastCharOfPatternInClassName);
                        if (isLastElementInList(part, patternParts) && (classNamePart.length() > 0) && mustEndWithPatternWord) {
                            return false;
                        }
                    } else {
                        classNamePart = "";
                    }
                } else {
                    return false;
                }
            } else {
                if (getPositionOfWildcardPartInString(part, classNamePart) >= 0) {
                    int positionOfLastCharOfPatternInClassName = getPositionOfWildcardPartInString(part, classNamePart) + part.length();
                    if (positionOfLastCharOfPatternInClassName != classNamePart.length() - 1) {
                        classNamePart = classNamePart.substring(positionOfLastCharOfPatternInClassName);
                        if (isLastElementInList(part, patternParts) && (classNamePart.length() > 0) && mustEndWithPatternWord) {
                            return false;
                        }
                    } else {
                        classNamePart = "";
                    }
                } else {
                    return false;
                }
            }
        }
        return true;
    }

    boolean isLastElementInList(String element, List<String> list) {
        String lastElement = list.get(list.size() - 1);
        return element.equals(lastElement);
    }

    boolean containsWildcard(String element) {
        return element.contains("*");
    }
}
