import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.*;

public class SorterByPatternTest {
    private SorterByPattern sorterByPattern = new SorterByPattern();

    @Test
    public void returnsTrueIfInputIsLowerCase() {
        assertTrue(sorterByPattern.isOnlyLowerCase("abcdefg"));
        assertTrue(sorterByPattern.isOnlyLowerCase("abc.de.fg"));
        assertTrue(sorterByPattern.isOnlyLowerCase("f.o.obar"));
        assertTrue(sorterByPattern.isOnlyLowerCase("üõäö"));
        assertTrue(sorterByPattern.isOnlyLowerCase("       abcdefg"));
        assertTrue(sorterByPattern.isOnlyLowerCase("   randomfilename"));
    }

    @Test
    public void returnsFalseIfInputHasUpperCaseCharacters() {
        assertFalse(sorterByPattern.isOnlyLowerCase("Abcdefg"));
        assertFalse(sorterByPattern.isOnlyLowerCase("abc.De.fg"));
        assertFalse(sorterByPattern.isOnlyLowerCase("f.O.obar"));
        assertFalse(sorterByPattern.isOnlyLowerCase("üõäÕ"));
        assertFalse(sorterByPattern.isOnlyLowerCase("       aBcDefg"));
        assertFalse(sorterByPattern.isOnlyLowerCase("   RandomFileName"));
    }

    @Test
    public void returnsTrueIfNameMatchesPattern_OnlyLowercase() {
        assertTrue(sorterByPattern.doesClassNameMatchLowerCasePattern("abcdefg", "aceg"));
        assertTrue(sorterByPattern.doesClassNameMatchLowerCasePattern("abc.de.fg", "aceg"));
        assertTrue(sorterByPattern.doesClassNameMatchLowerCasePattern("abc.de.fg", "adf"));
        assertTrue(sorterByPattern.doesClassNameMatchLowerCasePattern("abc.de.fg", "de"));
        assertTrue(sorterByPattern.doesClassNameMatchLowerCasePattern("       abcdefg", "aceg"));
        assertTrue(sorterByPattern.doesClassNameMatchLowerCasePattern("       abcdefg", "a"));
        assertTrue(sorterByPattern.doesClassNameMatchLowerCasePattern("       abcdefg", "g"));
        assertTrue(sorterByPattern.doesClassNameMatchLowerCasePattern("ü", "ü"));
        assertTrue(sorterByPattern.doesClassNameMatchLowerCasePattern("üõäö", "ü"));
        assertTrue(sorterByPattern.doesClassNameMatchLowerCasePattern("üõäö", "ö"));
    }

    @Test
    public void findsMatchingClassnameFromList() {
        assertEquals(Collections.singletonList("a.b.FooBarBaz"), sorterByPattern.getMatchingLowerCaseNames(Arrays.asList("a.b.FooBarBaz", "c.d.FooBar"), "fbb"));
    }

    @Test
    public void findsMatchingClassnameFromList_CamelCase() {
        assertEquals(Collections.singletonList("a.b.FooBarBaz"), sorterByPattern.getMatchingCamelCaseNames(Arrays.asList("a.b.FooBarBaz", "c.d.FooBar"), Arrays.asList("ar","Ba"), false));
    }

    @Test
    public void findsMatchingClassname_OnlyLowercase() {
        assertEquals(Collections.singletonList("a.b.FooBarBaz"), sorterByPattern.sort(Arrays.asList("a.b.FooBarBaz", "c.d.FooBar"), "fbb"));
        assertEquals(Arrays.asList("codeborne.WishMaker", "codeborne.MindReader"), sorterByPattern.sort(Arrays.asList("a.b.FooBarBaz", "c.d.FooBar", "codeborne.WishMaker", "codeborne.MindReader"), "dbn"));
    }

    @Test
    public void findsMatchingClassname_camelCase() {
        assertEquals(Collections.singletonList("a.b.FooBarBaz"), sorterByPattern.sort(Arrays.asList("a.b.FooBarBaz", "c.d.FooBar"), "arBa"));
        assertEquals(Collections.singletonList("codeborne.WishMaker"), sorterByPattern.sort(Arrays.asList("a.b.FooBarBaz", "c.d.FooBar", "codeborne.WishMaker", "codeborne.MindReader"), "borWiMa"));
    }

    @Test
    public void findsMatchingClassname_mustEndSameAsPattern() {
        assertEquals(Collections.singletonList("a.b.FooBarBaz"), sorterByPattern.sort(Arrays.asList("a.b.FooBarBaz", "c.d.FooBar"), "arBa "));
    }

    @Test
    public void findsMatchingClassname_withWildcard() {
        assertEquals(Collections.singletonList("codeborne.WishMaker"), sorterByPattern.sort(Arrays.asList("a.b.FooBarBaz", "c.d.FooBar", "codeborne.WishMaker", "codeborne.MindReader"), "borW*sMa"));

    }

    @Test
    public void returnsFalseIfNameDoesntMatchPattern_OnlyLowercase() {
        assertFalse(sorterByPattern.doesClassNameMatchLowerCasePattern("abcdefg", "ge"));
        assertFalse(sorterByPattern.doesClassNameMatchLowerCasePattern("abc.de.fg", "feb"));
        assertFalse(sorterByPattern.doesClassNameMatchLowerCasePattern("abc.de.fg", "aa"));
        assertFalse(sorterByPattern.doesClassNameMatchLowerCasePattern("abc.de.fg", "acfe"));
        assertFalse(sorterByPattern.doesClassNameMatchLowerCasePattern("       abcdefg", "gfa"));
        assertFalse(sorterByPattern.doesClassNameMatchLowerCasePattern("       abcdefg", "j"));
        assertFalse(sorterByPattern.doesClassNameMatchLowerCasePattern("üõäö", "q"));
        assertFalse(sorterByPattern.doesClassNameMatchLowerCasePattern("üõäö", "üw"));
    }

    @Test
    public void returnsTrueIfNameMatchesPattern_WithUppercase() {
        assertTrue(sorterByPattern.doesClassNameMatchCamelCasePattern("a.b.FooBarBaz", Arrays.asList("F", "B"), false));
        assertTrue(sorterByPattern.doesClassNameMatchCamelCasePattern("a.b.FooBarBaz", Arrays.asList("Fo", "Ba"), false));
        assertTrue(sorterByPattern.doesClassNameMatchCamelCasePattern("a.b.FooBarBaz", Arrays.asList("F", "Bar"), false));

        assertTrue(sorterByPattern.doesClassNameMatchCamelCasePattern("a.b.FooBar", Arrays.asList("F", "B"), false));
        assertTrue(sorterByPattern.doesClassNameMatchCamelCasePattern("a.b.FooBar", Arrays.asList("Fo", "Ba"), false));
        assertTrue(sorterByPattern.doesClassNameMatchCamelCasePattern("a.b.FooBar", Arrays.asList("F", "Bar"), false));
    }

    @Test
    public void returnsTrueIfNameMatchesPattern_WithUppercase_WithWildcard() {
        assertTrue(sorterByPattern.doesClassNameMatchCamelCasePattern("a.b.FooBarBaz", Arrays.asList("F", "B*r"), false));
        assertTrue(sorterByPattern.doesClassNameMatchCamelCasePattern("a.b.FooBarBaz", Arrays.asList("F*o", "Ba"), false));
        assertTrue(sorterByPattern.doesClassNameMatchCamelCasePattern("a.b.FooBar", Arrays.asList("F", "B*r"), false));
    }

    @Test
    public void returnsFalseIfNameDoesntMatchPattern_WithUppercase() {
        assertFalse(sorterByPattern.doesClassNameMatchCamelCasePattern("c.d.FooBar", Arrays.asList("B", "F"), false));
        assertFalse(sorterByPattern.doesClassNameMatchCamelCasePattern("FooBarBaz", Arrays.asList("f", "B", "b"), false));
    }

    @Test
    public void returnsTrueIfNameMatchesPattern_WithUppercase_WithWildcard_NameEndsWithPatternWord() {
        assertTrue(sorterByPattern.doesClassNameMatchCamelCasePattern("a.b.FooBar", Arrays.asList("F", "B*r"), true));
    }

    @Test
    public void returnsTrueIfNameMatchesPattern_WithUppercase_NameEndsWithPatternWord() {
        assertTrue(sorterByPattern.doesClassNameMatchCamelCasePattern("a.b.FooBar", Arrays.asList("F", "Bar"), true));
    }

    @Test
    public void returnsFalseIfNameMatchesPattern_WithUppercase_NameDoesntEndWithPatternWord() {
        assertFalse(sorterByPattern.doesClassNameMatchCamelCasePattern("FooBarBaz", Arrays.asList("F", "Bar"), true));
    }

    @Test
    public void returnsTrueIfCharacterIsPresentInString() {
        assertTrue(sorterByPattern.charExistsInString('a', "abcd"));
        assertTrue(sorterByPattern.charExistsInString('c', "Ab.cd"));
        assertTrue(sorterByPattern.charExistsInString('d', "Abcd"));
    }

    @Test
    public void returnsFalseIfCharacterIsNotPresentInString() {
        assertFalse(sorterByPattern.charExistsInString('q', "abcd"));
        assertFalse(sorterByPattern.charExistsInString('q', "ABCD"));
    }

    @Test
    public void returnsTrueIfSubstringIsPresentInString() {
        assertTrue(sorterByPattern.patternPartExistsInString("Ba", "a.b.FooBarBaz"));
        assertTrue(sorterByPattern.patternPartExistsInString("F", "a.b.FooBarBaz"));
        assertTrue(sorterByPattern.patternPartExistsInString("Fo", "a.b.FooBarBaz"));
        assertTrue(sorterByPattern.patternPartExistsInString("Bar", "a.b.FooBarBaz"));
    }

    @Test
    public void returnsFalseIfSubstringIsNotPresentInString() {
        assertFalse(sorterByPattern.patternPartExistsInString("Faa", "a.b.FooBarBaz"));
    }

    @Test
    public void returnsTrueIfWordMatchesPattern_LowerCase() {
        assertTrue(sorterByPattern.doesClassNameMatchLowerCasePattern("foobarbaz", "fbb"));
    }

    @Test
    public void returnsFalseIfWordDoesntMatchPattern_LowerCase() {
        assertFalse(sorterByPattern.doesClassNameMatchLowerCasePattern("foobarbaz", "bfb"));
    }

    @Test
    public void returnsTrueIfIsLastElementInList() {
        assertTrue(sorterByPattern.isLastElementInList("last", Arrays.asList("first", "second", "last")));
    }

    @Test
    public void returnsFalseIfIsNotLastElementInList() {
        assertFalse(sorterByPattern.isLastElementInList("second", Arrays.asList("first", "second", "last")));
    }

    @Test
    public void returnsTrueIfWordContainsAsterisk() {
        assertTrue(sorterByPattern.containsWildcard("abcd*dsre"));
        assertTrue(sorterByPattern.containsWildcard("C*rr"));
    }

    @Test
    public void returnsFalseIfWordDoesntContainAsterisk() {
        assertFalse(sorterByPattern.containsWildcard("abcd"));
        assertFalse(sorterByPattern.containsWildcard("aBCd"));
    }

    @Test
    public void returnsPositionOfSubstringWithWildcardInString() {
        assertEquals(4, sorterByPattern.getPositionOfWildcardPartInString("c*r", "wildcard"));
    }
}
