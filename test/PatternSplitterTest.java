import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

public class PatternSplitterTest {
    private PatternSplitter splitter = new PatternSplitter();

    @Test
    public void splitsStringIntoWordsWithSpaces() {
        assertEquals("a Test Assignment", splitter.splitWithSpacesIntoOneString("aTestAssignment"));
        assertEquals("a T Test A Assignment With D Double Letters", splitter.splitWithSpacesIntoOneString("aTTestAAssignmentWithDDoubleLetters"));
    }

    @Test
    public void splitsStringIntoList() {
        assertEquals(Arrays.asList("a", "Test", "Assignment"), splitter.splitIntoList("aTestAssignment"));
        assertEquals(Arrays.asList("a", "T", "Test", "A", "Assignment"), splitter.splitIntoList("aTTestAAssignment"));
    }
}
