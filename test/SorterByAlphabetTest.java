import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

public class SorterByAlphabetTest {
    private SorterByAlphabet sorter = new SorterByAlphabet();

    @Test
    public void returnsSortedList() {
        assertEquals(Arrays.asList("ant", "Coriander", "little", "Weasel"), sorter.sort(Arrays.asList("Coriander", "Weasel", "ant", "little")));
    }
}
